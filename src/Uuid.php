<?php
namespace Dracoder;

use Dracoder\Exceptions\Handlers\DumpExceptionHandler;
use Dracoder\Exceptions\Handlers\ExceptionHandler;
use Exception;

/**
 * Class Uuid
 *
 * @package App\Util
 */
class Uuid
{
    /**
     * Generate Uuid
     *
     * @param ExceptionHandler $exceptionHandler
     *
     * @return string
     */
    public static function uuid(ExceptionHandler $exceptionHandler = null): ?string
    {
        try {
            return \Ramsey\Uuid\Uuid::uuid4()->toString();
        } catch (Exception $e) {
            if (!$exceptionHandler) {
                $exceptionHandler = new DumpExceptionHandler($e, true);
            }
            $exceptionHandler->handle();
            return null;
        }
    }
}